{
    "type": "APL",
    "version": "1.0",
    "settings": {},
    "theme": "dark",
    "import": [
        {
            "name": "alexa-layouts",
            "version": "1.0.0"
        }
    ],
    "resources": [
        {
            "description": "Stock color for the light theme",
            "colors": {
                "colorTextPrimary": "#151920"
            }
        },
        {
            "description": "Stock color for the dark theme",
            "when": "${viewport.theme == 'dark'}",
            "colors": {
                "colorTextPrimary": "#f0f1ef"
            }
        },
        {
            "description": "Standard font sizes",
            "dimensions": {
                "textSizeBody": 48,
                "textSizePrimary": 27,
                "textSizeSecondary": 23,
                "textSizeDetails": 20,
                "textSizeSecondaryHint": 25
            }
        },
        {
            "description": "Common spacing values",
            "dimensions": {
                "spacingThin": 6,
                "spacingSmall": 12,
                "spacingMedium": 24,
                "spacingLarge": 48,
                "spacingExtraLarge": 72
            }
        },
        {
            "description": "Common margins and padding",
            "dimensions": {
                "marginTop": 40,
                "marginLeft": 60,
                "marginRight": 60,
                "marginBottom": 40
            }
        }
    ],
    "styles": {
        "textStyleBase": {
            "description": "Base font description; set color",
            "values": [
                {
                    "color": "@colorTextPrimary"
                }
            ]
        },
        "textStyleBase0": {
            "description": "Thin version of basic font",
            "extend": "textStyleBase",
            "values": {
                "fontWeight": "100"
            }
        },
        "textStyleBase1": {
            "description": "Light version of basic font",
            "extend": "textStyleBase",
            "values": {
                "fontWeight": "300"
            }
        },
        "textStyleBase2": {
            "description": "Regular version of basic font",
            "extend": "textStyleBase",
            "values": {
                "fontWeight": "500"
            }
        },
        "mixinBody": {
            "values": {
                "fontSize": "@textSizeBody"
            }
        },
        "mixinPrimary": {
            "values": {
                "fontSize": "@textSizePrimary"
            }
        },
        "mixinDetails": {
            "values": {
                "fontSize": "@textSizeDetails"
            }
        },
        "mixinSecondary": {
            "values": {
                "fontSize": "@textSizeSecondary"
            }
        },
        "textStylePrimary": {
            "extend": [
                "textStyleBase1",
                "mixinPrimary"
            ]
        },
        "textStyleSecondary": {
            "extend": [
                "textStyleBase0",
                "mixinSecondary"
            ]
        },
        "textStyleBody": {
            "extend": [
                "textStyleBase1",
                "mixinBody"
            ]
        },
        "textStyleSecondaryHint": {
            "values": {
                "fontFamily": "Bookerly",
                "fontStyle": "italic",
                "fontSize": "@textSizeSecondaryHint",
                "color": "@colorTextPrimary"
            }
        },
        "textStyleDetails": {
            "extend": [
                "textStyleBase2",
                "mixinDetails"
            ]
        }
    },
    "onMount": [],
    "graphics": {},
    "commands": {},
    "layouts": {
        "FullHorizontalListItem": {
            "parameters": [
                "listLength"
            ],
            "item": [
                {
                    "type": "Container",
                    "height": "100vh",
                    "width": "100vw",
                    "alignItems": "center",
                    "justifyContent": "end",
                    "items": [
                        {
                            "type": "Image",
                            "opacity": 0.3,
                            "width": "100vw",
                            "height": "100vh",
                            "overlayColor": "rgba(0, 0, 0, 0.6)",
                            "source": "${backgroundImage}",
                            "scale": "best-fill",
                            "position": "absolute"
                        },
                        {
                            "type": "AlexaHeader",
                            "headerTitle": "${title}",
                            "headerAttributionImage": "${logo}",
                            "grow": 1
                        },
                        {
                            "type": "Text",
                            "text": "${data.textContent.primaryText.text}",
                            "style": "textStyleBody",
                            "maxLines": 1
                        },
                        {
                            "type": "Text",
                            "text": "${data.textContent.secondaryText.text}",
                            "style": "textStyleDetails"
                        },
                        {
                            "type": "Text",
                            "text": "${ordinal} | ${listLength}",
                            "paddingBottom": "20dp",
                            "color": "white",
                            "spacing": "5dp"
                        }
                    ]
                }
            ]
        },
        "HorizontalListItem": {
            "item": [
                {
                    "type": "Container",
                    "width": "90vw",
                    "height": "100%",
                    "items": [
                        {
                            "type": "Text",
                            "style": "textStyleDetails",
                            "text": "${data.textContent.secondaryText.text}",
                            "fontSize": "30"
                        },
                        {
                            "type": "Sequence",
                            "height": "70vh",
                            "data": "${data.listTemplate1ListData.listPage.listItems}",
                            "items": [
                                {
                                    "type": "VerticalListItem",
                                    "ordinal": "${data.ordinalNumber}",
                                    "parameterTitle": "${data.textContent.parameterTitle.text}",
                                    "parameterSubtitle": "${data.textContent.parameterSubtitle.text}",
                                    "total": "${data.textContent.total.text}"
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        "ListTemplate2": {
            "parameters": [
                "backgroundImage",
                "title",
                "logo",
                "hintText",
                "listData"
            ],
            "items": [
                {
                    "when": "${viewport.shape == 'round'}",
                    "type": "Container",
                    "height": "100%",
                    "width": "100%",
                    "items": [
                        {
                            "type": "Image",
                            "opacity": 0.3,
                            "width": "100vw",
                            "height": "100vh",
                            "source": "${backgroundImage}",
                            "scale": "best-fill",
                            "position": "absolute"
                        },
                        {
                            "type": "AlexaHeader",
                            "headerTitle": "${title}",
                            "headerAttributionImage": "${logo}"
                        },
                        {
                            "type": "Sequence",
                            "scrollDirection": "horizontal",
                            "numbered": true,
                            "data": "${listData}"
                        }
                    ]
                },
                {
                    "type": "Container",
                    "height": "100vh",
                    "width": "100vw",
                    "items": [
                        {
                            "type": "Image",
                            "opacity": 0.3,
                            "width": "100vw",
                            "height": "100vh",
                            "source": "${backgroundImage}",
                            "scale": "best-fill",
                            "position": "absolute"
                        },
                        {
                            "type": "AlexaHeader",
                            "headerTitle": "${title}",
                            "headerAttributionImage": "${logo}"
                        },
                        {
                            "type": "Sequence",
                            "width": "100vw",
                            "height": "70vh",
                            "paddingLeft": "5vw",
                            "scrollDirection": "horizontal",
                            "data": "${listData}",
                            "numbered": true,
                            "item": [
                                {
                                    "type": "HorizontalListItem"
                                }
                            ]
                        },
                        {
                            "type": "AlexaFooter",
                            "footerHint": "${payload.listTemplate2ListData.hintText}",
                            "position": "absolute",
                            "top": "85vh"
                        }
                    ]
                }
            ]
        },
        "VerticalListItem": {
            "parameters": [
                "ordinal",
                "parameterTitle",
                "parameterSubtitle",
                "total"
            ],
            "item": [
                {
                    "when": "${viewport.shape == 'round'}",
                    "type": "Container",
                    "direction": "row",
                    "height": 200,
                    "width": 500,
                    "alignItems": "center",
                    "items": [
                        {
                            "type": "Text",
                            "text": "${ordinal}",
                            "paddingBottom": "20dp",
                            "color": "white",
                            "spacing": "5dp"
                        },
                        {
                            "type": "Container",
                            "direction": "column",
                            "spacing": 25,
                            "items": [
                                {
                                    "type": "Text",
                                    "style": "textStyleDetail",
                                    "width": "80vw",
                                    "grow": 1,
                                    "shrink": 1,
                                    "text": "${parameterTitle}",
                                    "fontWeight": "300",
                                    "maxLines": 2
                                },
                                {
                                    "type": "Text",
                                    "text": "${parameterSubtitle}",
                                    "style": "textStyleCaption",
                                    "fontWeight": "300",
                                    "grow": 1,
                                    "shrink": 1,
                                    "maxLines": 1
                                },
                                {
                                    "type": "Text",
                                    "text": "${total}",
                                    "style": "textStyleDetails",
                                    "fontWeight": "300",
                                    "grow": 1,
                                    "shrink": 1,
                                    "maxLines": 1
                                }
                            ]
                        }
                    ]
                },
                {
                    "type": "Container",
                    "width": "100vw",
                    "height": "150",
                    "direction": "row",
                    "alignItems": "center",
                    "items": [
                        {
                            "type": "Text",
                            "paddingBottom": "20dp",
                            "spacing": "5dp",
                            "text": "${ordinal}",
                            "color": "white"
                        },
                        {
                            "type": "Container",
                            "width": "70vw",
                            "spacing": 30,
                            "direction": "column",
                            "items": [
                                {
                                    "type": "Text",
                                    "style": "textStyleBody",
                                    "width": "70vw",
                                    "grow": 1,
                                    "shrink": 1,
                                    "text": "${parameterTitle}",
                                    "fontWeight": "300",
                                    "maxLines": 2,
                                    "fontSize": "30"
                                },
                                {
                                    "type": "Text",
                                    "style": "textStyleDetails",
                                    "width": "70vw",
                                    "grow": 1,
                                    "shrink": 1,
                                    "text": "${parameterSubtitle}",
                                    "fontWeight": "300",
                                    "maxLines": 1
                                }
                            ]
                        },
                        {
                            "type": "Text",
                            "style": "textStyleBody",
                            "grow": 1,
                            "shrink": 1,
                            "text": "${total}",
                            "fontWeight": "normal",
                            "textAlign": "left",
                            "maxLines": 1
                        }
                    ]
                }
            ]
        }
    },
    "mainTemplate": {
        "parameters": [
            "payload"
        ],
        "item": [
            {
                "type": "ListTemplate2",
                "backgroundImage": "${payload.listTemplate2Metadata.backgroundImage.sources[0].url}",
                "title": "${payload.listTemplate2Metadata.title}",
                "hintText": "${payload.listTemplate2Metadata.hintText}",
                "logo": "${payload.listTemplate2Metadata.logoUrl}",
                "listData": "${payload.listTemplate2ListData.listPage.listItems}"
            }
        ]
    }
}