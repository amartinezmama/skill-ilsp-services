module.exports = {
    // these are the permissions needed to get the first name
    GIVEN_NAME_PERMISSION: 'alexa::profile:given_name:read',
    // these are the permissions needed to get email
    GIVEN_EMAIL_PERMISSION: 'alexa::profile:email:read',
    // these are the permissions needed to send reminders
    REMINDERS_PERMISSION: 'alexa::alerts:reminders:skill:readwrite',
    // max number of entries to fetch from the external API
    MAX_BIRTHDAYS: 5,
    // APL documents
    APLDocs: {
        launchScreen: require('./Documents/launchScreen.json'),
        simpleScreen: require('./Documents/simpleScreen.json'),
        cardSingleInfoScreen: require('./Documents/cardSingleInfoScreen.json'),
        verticalGridScreen: require('./Documents/verticalGridScreen.json'),
        multiGridScreen: require('./Documents/multiGridScreen.json'),
        multiGridDetailsScreen: require('./Documents/multiGridDetailsScreen.json')
    },
    urlServiceBitacora: 'http://104.198.153.171/EnterpriseServices/ilsp-sa/Alexa/v1/',
    DEFAULT_timezone: 'America/Mexico_City',
    REPLACE: [
        '</say-as>',
        '<say-as interpret-as="cardinal">', 
        '<say-as interpret-as="digits">', 
        '<say-as interpret-as="spell-out">'
    ]
}