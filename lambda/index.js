// This sample demonstrates handling intents from an Alexa skill using the Alexa Skills Kit SDK (v2).
// Please visit https://alexa.design/cookbook for additional examples on implementing slots, dialog management,
// session persistence, api calls, and more.
const Alexa = require('ask-sdk-core');
const persistence = require('./persistence');
const interceptors = require('./interceptors');
const handlers = require('./handlers');

exports.handler = Alexa.SkillBuilders.custom()
.addRequestHandlers(
    handlers.LaunchRequestHandler, 
    handlers.RegisterDeviceIntentHandler,
    //Intents
    handlers.StatusCurrentServicesIntentHandler,
    handlers.SummaryByTransportLineIntentHandler,
    handlers.DetailsByShipmentNumberIntentHandler,
    handlers.DepartureStatusLastQuarterIntentHandler,
    handlers.CurrentSecurityStatusIntentHandler,
    handlers.DetailsAnomaliesByTransportLineIntentHandler,
    handlers.AnomalyStatusByTransportLineIntentHandler,
    handlers.StatusAnolamiesByOperatorIntentHandler,
    handlers.StatusAnomaliesByShipmentNumberIntentHandler,
    handlers.LastTrimesterAnomalyStatusIntentHandler,
    handlers.TheftStatusLastQuarterIntentHandler,
    //Intents
    handlers.TouchIntentHandler,
    handlers.HelpIntentHandler, 
    handlers.CancelAndStopIntentHandler, 
    handlers.FallbackIntentHandler, 
    handlers.SessionEndedRequestHandler, 
    handlers.IntentReflectorHandler)
.addErrorHandlers(handlers.ErrorHandler)
.addRequestInterceptors(
    interceptors.LocalizationRequestInterceptor, 
    interceptors.LoggingRequestInterceptor, 
    interceptors.LoadAttributesRequestInterceptor)
.addResponseInterceptors(
    interceptors.LoggingResponseInterceptor, 
    interceptors.SaveAttributesResponseInterceptor)
.withPersistenceAdapter(
    persistence.getPersistenceAdapter())
.withApiClient(new Alexa.DefaultApiClient()).lambda();
