const POSITIVE_SOUND = `<audio src='soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_positive_response_02'/>`;
const RESPONSE_SUCCESS = `<audio src="soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_neutral_response_01"/>`;
const RESPONSE_ERROR = `<audio src="soundbank://soundlibrary/ui/gameshow/amzn_ui_sfx_gameshow_negative_response_01"/>`;
const GREETING_SPEECHON = '<say-as interpret-as="interjection">felicidades</say-as>';

module.exports = {
    en: {
        translation: {
        }
    },
    es: {
        translation: {
            WELCOME_MSG: ['¡Hola {{name}}! Dime. ¿En qué te puedo ayudar? '],
            WELCOME_UNREGISTERED_DEVICE_MSG: ['¡Hola {{name}}! '],
            HELP_MSG: 'Puedes preguntarme información general de viajes, servicios o bitácoras y números de envío. ',
            GOODBYE_MSG: ['¡Hasta luego {{name}}! ', '¡Adios {{name}}! ', '¡Hasta pronto {{name}}! ', '¡Nos vemos {{name}}! ', '<say-as interpret-as="interjection">nos vemos luego</say-as>', '<say-as interpret-as="interjection">buena suerte</say-as>'],
            REFLECTOR_MSG: 'Acabas de activar {{intent}} ',
            FALLBACK_MSG: ['<say-as interpret-as="interjection">lo lamento mucho</say-as>, no sé nada sobre eso. Por favor inténtalo otra vez. ', '<say-as interpret-as="interjection">lo siento mucho</say-as>, no sé nada sobre eso. Por favor inténtalo otra vez. '],
            ERROR_MSG: ['<say-as interpret-as="interjection">lo lamento mucho</say-as>, ha habido un problema. Por favor inténtalo otra vez. ', '<say-as interpret-as="interjection">lo siento mucho</say-as>, ha habido un problema. Por favor inténtalo otra vez. '],
            UNSUPPORTED_DEVICE_MSG: 'Este dispositivo no soporta la operación que estás intentando realizar. ',
            MISSING_PERMISSION_MSG: 'Parece que no has autorizado el acceso a tu nombre y correo electrónico. Te recuerdo que es indispensable la autorización para mi correcto funcionamiento. Te he enviado una tarjeta a la app de Alexa para que los habilites. ',
            CANCEL_MSG: 'Esta bien. Lo cancelamos. ',
            NOT_DATA_FOUND_MSG: 'No pudimos encontrar el dato que buscas.  ',
            LAUNCH_HEADER_MSG: 'Servicios de bitácoras',
            LAUNCH_HINT_MSG: ['¡Ayuda!'],
            SHORT_HELP_MSG: 'Dime que otra cosa quieres hacer. ',
            PROGRESSIVE_MSG: ['<say-as interpret-as="interjection">hmm</say-as>. ', 'Espera un momento..', '<say-as interpret-as="interjection">pérame tantito</say-as>. ', 'Espera un momento mientras busco la información en el sistema. '],
            PROGRESSIVE_REGISTER_DEVICE_MSG: 'Espera un momento mientras terminamos tu registro. ',
            RESPONSE_SUCCESS_MSG: RESPONSE_SUCCESS,
            RESPONSE_ERROR_MSG: RESPONSE_ERROR,
            CONJUNCTION_MSG: ' y ',
            REGISTER_DEVICE: '',
            MISSING_DEVICE_NOT_BE_REGISTERED: 'Parece que aún no has registrado tu dispositivo. Prueba diciendo, registra mi dispositivo. ',
            DEVICE_REGISTERED_SUCCESSFULLY: 'Su dispositivo se registró con éxito. ',
            DEVICE_NOT_BE_REGISTERED: 'No se pudo registrar el dispositivo. ',
            DEVICE_ALREADY_REGISTERED: 'Su dispositivo ya se encuentra registrado. ',
            ERROR_CONTACT_TO_ILSP: 'Contacta a ILSP. ',
            THE_SHIPMENT_NUMBER: 'Número de envío <say-as interpret-as="spell-out">{{shipmentNumber}}</say-as> ',
            DEPARTURES_BY_CLIENT_VS_DESTINATION_MSG: 'Salidas por Cliente/Destino',
            SALIDAS_POR_LINEA_TRANSPORTISTA_MSG: 'Salidas por Línea Transportista',
            SERVICES_IN_VALIDATION_BY_REACTION_MSG: 'Servicios en validación por reacción',
            SERVICES_IN_THE_PROCESS_OF_THEFT_MSG: 'Servicios en proceso de robo',
            MOST_ANOMALOUS_TRANSPORT_LINES_MSG: 'Líneas transportistas más anómalas',
            MOST_ANOMALOUS_OPERATORS_MSG: 'Operadores más anómalos',
            TRANSPORT_LINES_WITH_THE_MOST_THEFTS_MSG: 'Líneas transportistas con más robos',
            DESTINATIONS_WITH_THE_MOST_THEFTS_MSG: 'Destinos con más robos',
            DETAILS_MSG: 'Detalles',
            //Section StatusCurrentServicesIntent
            StatusCurrentServicesIntent_DATA_MSG: 'Actualmente se {{StatusCurrentServicesIntent_DATA_TOTAL_RESULT}}, {{StatusCurrentServicesIntent_DATA_TYPES_CONJUNCTION_RESULT}}. {{StatusCurrentServicesIntent_DATA_RISK_ARRIVING_RESULT}} ',
            StatusCurrentServicesIntent_DATA_TOTAL_MSG: 'tiene <say-as interpret-as="cardinal">{{count}}</say-as> bitácora',
            StatusCurrentServicesIntent_DATA_TOTAL_MSG_plural: 'tienen <say-as interpret-as="cardinal">{{count}}</say-as> bitácoras',
            StatusCurrentServicesIntent_DATA_TYPES_CONJUNCTION_MSG: '{{StatusCurrentServicesIntent_DATA_FOREIGN_RESULT}} y {{StatusCurrentServicesIntent_DATA_LOCALS_RESULT}}',
            StatusCurrentServicesIntent_DATA_FOREIGN_MSG: 'de las cuáles <say-as interpret-as="cardinal">{{count}}</say-as> es foránea',
            StatusCurrentServicesIntent_DATA_FOREIGN_MSG_plural: 'de las cuáles <say-as interpret-as="cardinal">{{count}}</say-as> son foráneas',
            StatusCurrentServicesIntent_DATA_ALL_FOREIGN_MSG: 'Todos son foráneos',
            StatusCurrentServicesIntent_DATA_LOCALS_MSG: '<say-as interpret-as="cardinal">{{count}}</say-as> local',
            StatusCurrentServicesIntent_DATA_LOCALS_MSG_plural: '<say-as interpret-as="cardinal">{{count}}</say-as> locales',
            StatusCurrentServicesIntent_DATA_ALL_LOCALS_MSG: 'Todos son locales',
            StatusCurrentServicesIntent_DATA_RISK_ARRIVING_MSG: 'Además, {{StatusCurrentServicesIntent_DATA_RISK_ARRIVING_TOTAL_RESULT}} con riesgo de llegar tarde a su cita, lo que representa un <say-as interpret-as="cardinal">{{totalPercentageServicesRiskArrivingLate}}</say-as>%. ',
            StatusCurrentServicesIntent_DATA_RISK_ARRIVING_TOTAL_MSG: '<say-as interpret-as="cardinal">{{count}}</say-as> viaje',
            StatusCurrentServicesIntent_DATA_RISK_ARRIVING_TOTAL_MSG_plural: '<say-as interpret-as="cardinal">{{count}}</say-as> viajes',
            StatusCurrentServicesIntent_DATA_NO_RISK_ARRIVING_MSG: 'No tienes viajes con riesgo de llegar tarde a su cita. ',
            StatusCurrentServicesIntent_NO_DATA_MSG: 'Actualmente no cuentas con bitácoras. ',
            //Section StatusCurrentServicesIntent
            //Section SummaryByTransportLineIntent
            SummaryByTransportLineIntent_DATA_MSG: 'En este momento hay {{SummaryByTransportLineIntent_DATA_TOTAL_LT_RESULT}} con viajes. La línea transportista con más viajes es {{transportLineName}} con {{SummaryByTransportLineIntent_DATA_TOTAL_TS_RESULT}}. ',
            SummaryByTransportLineIntent_DATA_TOTAL_LT_MSG: '<say-as interpret-as="cardinal">{{count}}</say-as> línea transportista',
            SummaryByTransportLineIntent_DATA_TOTAL_LT_MSG_plural: '<say-as interpret-as="cardinal">{{count}}</say-as> líneas transportistas',
            SummaryByTransportLineIntent_DATA_TOTAL_TS_MSG: '<say-as interpret-as="cardinal">{{count}}</say-as> viaje',
            SummaryByTransportLineIntent_DATA_TOTAL_TS_MSG_plural: '<say-as interpret-as="cardinal">{{count}}</say-as> viajes',
            SummaryByTransportLineIntent_NO_DATA_MSG: 'No tienes viajes en este momento. ',
            //Section SummaryByTransportLineIntent
            //Section DetailsByShipmentNumberIntent
            DetailsByShipmentNumberIntent_DATA_MSG: 'El número de envío <say-as interpret-as="spell-out">{{shipmentNumber}}</say-as> lo lleva la línea transportista {{transportLineName}}, con el económico <say-as interpret-as="spell-out">{{economic}}</say-as>, y es conducido por el operador {{operatorName}}, con número de teléfono <say-as interpret-as="spell-out">{{operatorPhone}}</say-as>. Se encuentra en {{address}}. ',
            DetailsByShipmentNumberIntent_NO_DATA_MSG: 'No pude encontrar el número de envío que buscas. Verifica que el datos sea correcto. ',
            //Section DetailsByShipmentNumberIntent
            //Section DepartureStatusLastQuarterIntent
            DepartureStatusLastQuarterIntent_DATA_MSG: 'Tuviste un total de {{DepartureStatusLastQuarterIntent_DATA_TOTAL_RESULT}} en el último trimestre, {{DepartureStatusLastQuarterIntent_DATA_ONTIME_RESULT}}. Además, se tuvo un total de {{DepartureStatusLastQuarterIntent_DATA_TL_TOTAL_RESULT}}, siendo la línea {{worstTransportLineName}} con una calificación más baja. ',
            DepartureStatusLastQuarterIntent_DATA_TOTAL_MSG: '<say-as interpret-as="cardinal">{{count}}</say-as> salida',
            DepartureStatusLastQuarterIntent_DATA_TOTAL_MSG_plural: '<say-as interpret-as="cardinal">{{count}}</say-as> salidas',
            DepartureStatusLastQuarterIntent_DATA_TL_TOTAL_MSG: '<say-as interpret-as="cardinal">{{count}}</say-as> línea transportista',
            DepartureStatusLastQuarterIntent_DATA_TL_TOTAL_MSG_plural: '<say-as interpret-as="cardinal">{{count}}</say-as> líneas transportistas',
            DepartureStatusLastQuarterIntent_DATA_ONTIME_MSG: 'de los cuales {{DepartureStatusLastQuarterIntent_DATA_ONTIME_TOTAL_RESULT}} en tiempo, representando un <say-as interpret-as="cardinal">{{percentageTotalServicesOnTime}}</say-as>% del total de viajes',
            DepartureStatusLastQuarterIntent_DATA_ONTIME_TOTAL_MSG: '<say-as interpret-as="cardinal">{{count}}</say-as> llegó',
            DepartureStatusLastQuarterIntent_DATA_ONTIME_TOTAL_MSG_plural: '<say-as interpret-as="cardinal">{{count}}</say-as> llegaron',
            DepartureStatusLastQuarterIntent_DATA_NO_ONTIME_MSG: 'de los cuales ningun viaje llego en tiempo',
            DepartureStatusLastQuarterIntent_NO_DATA_MSG: 'No tuviste viajes en el último trimestre. ',
            //Section DepartureStatusLastQuarterIntent
            //Section CurrentSecurityStatusIntent
            CurrentSecurityStatusIntent_DATA_MSG: 'El viaje con número de envío <say-as interpret-as="spell-out">{{shipmentNumber}}</say-as> es tu viaje con mayor riesgo en este momento con <say-as interpret-as="cardinal">{{riskPercentage}}</say-as>%. {{CurrentSecurityStatusIntent_DATA_CONJUNCTION_RESULT}}',
            CurrentSecurityStatusIntent_DATA_CONJUNCTION_MSG: '{{CurrentSecurityStatusIntent_DATA_REACTION_RESULT}} y {{CurrentSecurityStatusIntent_DATA_THEFT_RESULT}}. ',
            CurrentSecurityStatusIntent_DATA_REACTION_MSG: 'Tienes <say-as interpret-as="cardinal">{{count}}</say-as> bitácora en validación por reacción',
            CurrentSecurityStatusIntent_DATA_REACTION_MSG_plural: 'Tienes <say-as interpret-as="cardinal">{{count}}</say-as> bitácoras en validación por reacción',
            CurrentSecurityStatusIntent_DATA_NO_REACTION_MSG: 'No tienes bitácoras en validación por reacción',
            CurrentSecurityStatusIntent_DATA_THEFT_MSG: '<say-as interpret-as="cardinal">{{count}}</say-as> bitácora en proceso de robo',
            CurrentSecurityStatusIntent_DATA_THEFT_MSG_plural: '<say-as interpret-as="cardinal">{{count}}</say-as> bitácoras en proceso de robo',
            CurrentSecurityStatusIntent_DATA_NO_THEFT_MSG: 'no tienes bitácoras en proceso de robo',
            CurrentSecurityStatusIntent_DATA_NO_REACTION_NO_THEFT_MSG: 'No tienes bitácoras en validación por reacción y no tienes bitácoras en proceso de robo. ',
            CurrentSecurityStatusIntent_NO_DATA_MSG: 'No tienes viajes con riesgo en este momento. ',
            //Section CurrentSecurityStatusIntent
            //Section DetailsAnomaliesByTransportLineIntent
            DetailsAnomaliesByTransportLineIntent_DATA_MSG: 'Actualmente tienes {{DetailsAnomaliesByTransportLineIntent_DATA_TOTAL_RESULT}}. {{DetailsAnomaliesByTransportLineIntent_DATA_CONJUNCTION_RESULT}}',
            DetailsAnomaliesByTransportLineIntent_DATA_TOTAL_MSG: '<say-as interpret-as="cardinal">{{count}}</say-as> anomalía',
            DetailsAnomaliesByTransportLineIntent_DATA_TOTAL_MSG_plural: '<say-as interpret-as="cardinal">{{count}}</say-as> anomalías',
            DetailsAnomaliesByTransportLineIntent_DATA_CONJUNCTION_MSG: 'Tu línea transportista mejor calificada es {{bestTransportLineName}} con un <say-as interpret-as="cardinal">{{percentageAnomalyOfBestTransportLine}}</say-as>% de anomalías y tu línea transportista peor calificada es {{worstTransportLineName}} con un <say-as interpret-as="cardinal">{{percentageAnomalyOfWorstTransportLine}}</say-as>% de anomalías. ',
            DetailsAnomaliesByTransportLineIntent_DATA_UNIQUE_TRANSPORTLINE_MSG: 'Tu única línea transportista es {{bestTransportLineName}} con un <say-as interpret-as="cardinal">{{percentageAnomalyOfBestTransportLine}}</say-as>% de anomalías',
            DetailsAnomaliesByTransportLineIntent_NO_DATA_MSG: 'Actualmente no tienes anomalías. ',
            //Section DetailsAnomaliesByTransportLineIntent
            //Section AnomalyStatusByTransportLineIntent
            AnomalyStatusByTransportLineIntent_DATA_MSG: 'La línea transportista {{transportLineName}} tiene un índice de <say-as interpret-as="cardinal">{{percentageTransportLine}}</say-as>% de anomalías. Su mayor anomalía es {{anomalyName}} y su operador más anómalo es {{operatorName}}. ',
            AnomalyStatusByTransportLineIntent_NO_DATA_MSG: 'No tienes líneas transportistas anómalas. ',
            //Section AnomalyStatusByTransportLineIntent
            //Section StatusAnolamiesByOperatorIntent
            StatusAnolamiesByOperatorIntent_DATA_MSG: 'En este momento tu operador mejor calificado es {{bestOperatorName}} con un <say-as interpret-as="cardinal">{{percentageBestOperator}}</say-as>% de anomalías y tu operador peor calificado es {{worstOperatorName}} con un <say-as interpret-as="cardinal">{{percentageWorstOperator}}</say-as>% de anomalías. ',
            StatusAnolamiesByOperatorIntent_DATA_UNIQUE_MSG: 'En este momento tu único operador es {{bestOperatorName}} con un <say-as interpret-as="cardinal">{{percentageBestOperator}}</say-as>% de anomalías. ',
            StatusAnolamiesByOperatorIntent_NO_DATA_MSG: 'No tienes operadores anómalos. ',
            //Section StatusAnolamiesByOperatorIntent
            //Section StatusAnomaliesByShipmentNumberIntent
            StatusAnomaliesByShipmentNumberIntent_DATA_MSG: 'El número de envío <say-as interpret-as="spell-out">{{shipmentNumber}}</say-as> tiene {{StatusAnomaliesByShipmentNumberIntent_DATA_TOTAL_RESULT}}. ',
            StatusAnomaliesByShipmentNumberIntent_DATA_TOTAL_MSG: '<say-as interpret-as="cardinal">{{count}}</say-as> anomalía',
            StatusAnomaliesByShipmentNumberIntent_DATA_TOTAL_MSG_plural: '<say-as interpret-as="cardinal">{{count}}</say-as> anomalías',
            StatusAnomaliesByShipmentNumberIntent_NO_DATA_MSG: 'No pude encontrar el número de envío que buscas. Verifica que el datos sea correcto. ',
            //Section StatusAnomaliesByShipmentNumberIntent
            //Section LastTrimesterAnomalyStatusIntent
            LastTrimesterAnomalyStatusIntent_DATA_MSG: 'En el último trimestre se tuvo un índice de <say-as interpret-as="cardinal">{{percentageServices}}</say-as>% de un total de {{LastTrimesterAnomalyStatusIntent_DATA_TOTAL_RESULT}}. ',
            LastTrimesterAnomalyStatusIntent_DATA_TOTAL_MSG: '<say-as interpret-as="cardinal">{{count}}</say-as> viaje',
            LastTrimesterAnomalyStatusIntent_DATA_TOTAL_MSG_plural: '<say-as interpret-as="cardinal">{{count}}</say-as> viajes',
            LastTrimesterAnomalyStatusIntent_NO_DATA_MSG: 'No tuviste viajes en el último trimestre. ',
            //Section LastTrimesterAnomalyStatusIntent
            //Section TheftStatusLastQuarterIntent
            TheftStatusLastQuarterIntent_DATA_MSG: 'Se {{TheftStatusLastQuarterIntent_DATA_TOTAL_RESULT}} que representa el <say-as interpret-as="cardinal">{{totalPercentageServicesValidated}}</say-as>% de salidas{{TheftStatusLastQuarterIntent_DATA_PHYSICAL_VALIDATIONS_RESULT}}. {{TheftStatusLastQuarterIntent_DATA_THEFT_RESULT}}. ',
            TheftStatusLastQuarterIntent_DATA_TOTAL_MSG: 'tuvo <say-as interpret-as="cardinal">{{count}}</say-as> validación de servicio',
            TheftStatusLastQuarterIntent_DATA_TOTAL_MSG_plural: 'tuvieron <say-as interpret-as="cardinal">{{count}}</say-as> validaciones de servicios',
            TheftStatusLastQuarterIntent_DATA_PHYSICAL_VALIDATIONS_MSG: ', de los cuáles {{TheftStatusLastQuarterIntent_DATA_PHYSICAL_VALIDATIONS_TOTAL_RESULT}} validación física de la unidad',
            TheftStatusLastQuarterIntent_DATA_PHYSICAL_VALIDATIONS_TOTAL_MSG: '<say-as interpret-as="cardinal">{{count}}</say-as> requirio',
            TheftStatusLastQuarterIntent_DATA_PHYSICAL_VALIDATIONS_TOTAL_MSG_plural: '<say-as interpret-as="cardinal">{{count}}</say-as> requirieron',
            TheftStatusLastQuarterIntent_DATA_NO_PHYSICAL_VALIDATIONS_MSG: '',
            TheftStatusLastQuarterIntent_DATA_THEFT_MSG: 'Por otro lado, se tuvo un total de {{TheftStatusLastQuarterIntent_DATA_THEFT_TOTAL_RESULT}} de robo{{TheftStatusLastQuarterIntent_DATA_THEFT_CONJUNCTION_RESULT}}. ',
            TheftStatusLastQuarterIntent_DATA_THEFT_TOTAL_MSG: '<say-as interpret-as="cardinal">{{count}}</say-as> intento',
            TheftStatusLastQuarterIntent_DATA_THEFT_TOTAL_MSG_plural: '<say-as interpret-as="cardinal">{{count}}</say-as> intentos',
            TheftStatusLastQuarterIntent_DATA_THEFT_CONJUNCTION_MSG: ', {{TheftStatusLastQuarterIntent_DATA_THEFT_CONSSUMATED_RESULT}} y {{TheftStatusLastQuarterIntent_DATA_THEFT_RECOVERED_RESULT}}',
            TheftStatusLastQuarterIntent_DATA_THEFT_CONSSUMATED_MSG: '{{TheftStatusLastQuarterIntent_DATA_THEFT_CONSSUMATED_TOTAL_RESULT}} teniendo un <say-as interpret-as="cardinal">{{totalPercentageConsummatedTheftAndSinister}}</say-as>% de siniestralidad',
            TheftStatusLastQuarterIntent_DATA_THEFT_CONSSUMATED_TOTAL_MSG: '<say-as interpret-as="cardinal">{{count}}</say-as> robo consumado',
            TheftStatusLastQuarterIntent_DATA_THEFT_CONSSUMATED_TOTAL_MSG_plural: '<say-as interpret-as="cardinal">{{count}}</say-as> robos consumados',
            TheftStatusLastQuarterIntent_DATA_THEFT_RECOVERED_MSG: '{{TheftStatusLastQuarterIntent_DATA_THEFT_RECOVERED_TOTAL_RESULT}} lo que indica un <say-as interpret-as="cardinal">{{totalPercentageServicesTheftRecovered}}</say-as>% recuperación',
            TheftStatusLastQuarterIntent_DATA_THEFT_RECOVERED_TOTAL_MSG: '<say-as interpret-as="cardinal">{{count}}</say-as> robo recuperado',
            TheftStatusLastQuarterIntent_DATA_THEFT_RECOVERED_TOTAL_MSG_plural: '<say-as interpret-as="cardinal">{{count}}</say-as> robos recuperados',
            TheftStatusLastQuarterIntent_DATA_NO_THEFT_MSG: 'No tuviste intentos de robo. ',
            TheftStatusLastQuarterIntent_NO_DATA_MSG: 'No se tuvieron validaciones de servicios. ',
            //Section TheftStatusLastQuarterIntent
            //Codigos de errores del Api
            SuccessOperation_MSG: 'Operación exitosa. ',
            SuccessRegistered_MSG: 'Su dispositivo se registró con éxito. ',
            SuccessAlreadyRegistered_MSG: 'El dispositivo ya se encuentra registrado. ',
            DeviceNotRegistered_MSG: 'El dispositivo no se ha registrado. ',
            DeviceCouldNotBeRegistered_MSG: 'El dispositivo no pudo ser registrado. ',
            ServiceNotAvailable_MSG: 'Servicio de ILSP no disponible. ',
            ClientsNotConfigured_MSG: 'No cuenta con clientes configurados',
            LAUNCH_IMAGES_MSG: ['https://cdn.stocksnap.io/stocksnap-user-uploads/138482-1598058709771.jpg','https://cdn.stocksnap.io/stocksnap-user-uploads/138482-1598059151027.jpg'],
            LAYOUT_IMAGES_MSG: ['https://cdn.stocksnap.io/stocksnap-user-uploads/138482-1598058709771.jpg','https://cdn.stocksnap.io/stocksnap-user-uploads/138482-1598059151027.jpg','https://cdn.stocksnap.io/stocksnap-user-uploads/138482-1598298888349.jpg','https://cdn.stocksnap.io/stocksnap-user-uploads/138482-1598298859384.jpg','https://cdn.stocksnap.io/stocksnap-user-uploads/138482-1598059198560.jpg'],
            LOGO_MSG: 'http://ilsp.mx/images-ilsp/log2.png'
        }
    }
}