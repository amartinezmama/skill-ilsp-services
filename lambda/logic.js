const moment = require('moment-timezone'); // will help us do all the birthday math
const constants = require('./constants');
const request = require('request');
const util = require('./util');

module.exports = {
    createReminderData(daysUntilBirthday, timezone, locale, message) {
        timezone = timezone ? timezone : constants.DEFAULT_timezone; // so it works on the simulator, replace with your timezone and remove if testing on a real device
        moment.locale(locale);
        const now = moment().tz(timezone);
        const scheduled = now.startOf('day').add(daysUntilBirthday, 'days');
        console.log('Reminder schedule: ' + scheduled.format('YYYY-MM-DDTHH:mm:00.000'));

        return {
            requestTime: now.format('YYYY-MM-DDTHH:mm:00.000'),
            trigger: {
                type: 'SCHEDULED_ABSOLUTE',
                scheduledTime: scheduled.format('YYYY-MM-DDTHH:mm:00.000'),
                timeZoneId: timezone
            },
            alertInfo: {
                spokenInfo: {
                    content: [
                        {
                            locale: locale,
                            text: message
                        }
                    ]
                }
            },
            pushNotification: {
                status: 'ENABLED'
            }
        }
    },
    callDirectiveService(handlerInput, msg) { // Call Alexa Directive Service.
        const {requestEnvelope} = handlerInput;
        const directiveServiceClient = handlerInput.serviceClientFactory.getDirectiveServiceClient();
        const requestId = requestEnvelope.request.requestId;
        const {apiEndpoint, apiAccessToken} = requestEnvelope.context.System;

        // build the progressive response directive
        const directive = {
            header: {
                requestId
            },
            directive: {
                type: 'VoicePlayer.Speak',
                speech: msg
            }
        };
        // send directive
        return directiveServiceClient.enqueue(directive, apiEndpoint, apiAccessToken);
    },
    getDatePartsArrival(year, month, day, hour, minutes, timezone) {
        const today = moment().tz(timezone).startOf('day');
        let arrivalDate = moment(`${month}/${day}/${year} ${hour}:${minutes}`, 'MM/DD/YYYY HH:MM').tz(timezone).startOf('day');

        if (today.isAfter(arrivalDate)) {
            return {hour: 0, minutes: 0};
        }

        const hoursToLate = today.diff(arrivalDate, 'hours');
        arrivalDate = moment(today).add(hoursToLate, 'hours');
        const minutesToLate = today.diff(arrivalDate, 'minutes');
        return {hour: hoursToLate, minutes: minutesToLate};
    },
    async validPermissions(handlerInput) {
        const {attributesManager, serviceClientFactory, requestEnvelope} = handlerInput;
        const sessionAttributes = attributesManager.getSessionAttributes();

        let PERMISSIONS = [];
        try {
            const {permissions} = requestEnvelope.context.System.user;
            if (!permissions) {
                PERMISSIONS = [constants.GIVEN_NAME_PERMISSION, constants.GIVEN_EMAIL_PERMISSION];
                throw {
                    statusCode : 401,
                    message : 'No permissions available'
                };
            }
            // there are zero permissions, no point in intializing the API
            const upsServiceClient = serviceClientFactory.getUpsServiceClient();
            const profileName = await upsServiceClient.getProfileGivenName();
            if (profileName) {
                sessionAttributes['name'] = profileName;
            } else {
                PERMISSIONS.push(constants.GIVEN_NAME_PERMISSION);
                throw {
                    statusCode : 401,
                    message : 'No permissions name available'
                };
            }

            const profileEmail = await upsServiceClient.getProfileEmail();
            if (profileEmail) {
                sessionAttributes['email'] = profileEmail;
            } else {
                PERMISSIONS.push(constants.GIVEN_EMAIL_PERMISSION);
                throw {
                    statusCode : 401,
                    message : 'No permissions email available'
                };
            }
            return {success: true, message: '', permissions: []};
        } catch (error) {
            console.log(JSON.stringify(error));
            if (error.statusCode === 401 || error.statusCode === 403) {
                // the user needs to enable the permissions for given name, let's send a silent permissions card.
                // voiceData = handlerInput.t('MISSING_PERMISSION_MSG');
                return {success: true, message: handlerInput.t('MISSING_PERMISSION_MSG'), permissions: PERMISSIONS};
                // return handlerInput.responseBuilder.speak(voiceData).withAskForPermissionsConsentCard(PERMISSIONS).getResponse();
            } else { // voiceData = handlerInput.t('ERROR_MSG');
                return {success: false, message: handlerInput.t('ERROR_MSG'), permissions: []};
            }
        }
    },
    validRegister(handlerInput) {
        const {attributesManager} = handlerInput;
        const sessionAttributes = attributesManager.getSessionAttributes();

        if (!sessionAttributes['deviceId']) {
            if (util.supportsAPL(handlerInput)) {
                return {success: false, message: handlerInput.t('MISSING_DEVICE_NOT_BE_REGISTERED')};
            } else {
                handlerInput.responseBuilder.addDelegateDirective({
                    name: 'RegisterDeviceIntent',
                    confirmationStatus: 'NONE',
                    slots: {}
                })
            }
        } else {
            return {success: true, message: ''};
        }
    },
    encodeBase64(data) {
        var b = Buffer.from(data);
        return b.toString('base64');
    },
    decodeBase64(data) {
        var b = Buffer.from(data, 'base64')
        return b.toString();
    },
    async httpGet(methodName, params) {
        console.log(params);
        const promise = new Promise(function (resolve, reject) {
            var url = `${constants.urlServiceBitacora}IlspAlexaServices/${methodName}`;
            if (params && params.length > 0) {
                params.forEach(parameter => {
                    var b = Buffer.from(parameter);
                    url += `/${b.toString('base64')}`;
                });
            }
            
            let options = {
                'method': 'GET',
                'url': url,
                'headers': {
                    'Content-Type': 'application/json;charset=UTF-8',
                    'Accept': 'application/json',
                    'Accept-Charset': 'utf-8'
                }
            };
            request(options, function (error, response) {
                if (error) {
                    reject(error);
                } else {
                    resolve(JSON.parse(response.body));
                }
            });
        })
        return promise;
    },
    async httpPost(methodName, params) {
        const promise = new Promise(function (resolve, reject) {
            var url = `${constants.urlServiceBitacora}IlspAlexaServices/${methodName}`;
            
            let options = {
                'method': 'POST',
                'url': url,
                'headers': {
                    'Content-Type': 'application/json;charset=UTF-8',
                    'Accept': 'application/json',
                    'Accept-Charset': 'utf-8'
                },
                body: JSON.stringify(params)
            }
            request(options, async function (error, response) {
                if (error) {
                    reject(error);
                } else {
                    resolve(JSON.parse(response.body));
                }
            });
        })
        return promise;
    },
    getLaunchScreen(handlerInput, content) {
        // Add APL Directive to response
        if (util.supportsAPL(handlerInput)) {
            handlerInput.responseBuilder.addDirective({
                type: 'Alexa.Presentation.APL.RenderDocument',
                version: '1.0',
                document: constants.APLDocs.launchScreen,
                datasources: {
                    launchData: {
                        type: 'object',
                        properties: {
                            backgroundImage: {
                                contentDescription: null,
                                smallSourceUrl: null,
                                largeSourceUrl: null,
                                sources: [
                                    {
                                        //url: handlerInput.t('LAYOUT_IMAGES_MSG'),
                                        url: handlerInput.t('LAUNCH_IMAGES_MSG'),
                                        size: "small",
                                        widthPixels: 0,
                                        heightPixels: 0
                                    },
                                    {
                                        //url: handlerInput.t('LAYOUT_IMAGES_MSG'),
                                        url: handlerInput.t('LAUNCH_IMAGES_MSG'),
                                        size: "large",
                                        widthPixels: 0,
                                        heightPixels: 0
                                    }
                                ]
                            },
                            logoUrl: handlerInput.t('LOGO_MSG'),
                            headerTitle: handlerInput.t('LAUNCH_HEADER_MSG'),
                            mainText: (function () { constants.REPLACE.forEach(rep => { content = content.replace(new RegExp(rep, 'g'), ''); }); return content; })(),
                            hintString: handlerInput.t('LAUNCH_HINT_MSG'),
                        },
                        transformers: [{
                            inputPath: 'hintString',
                            transformer: 'textToHint',
                        }]
                    }
                }
            });
        }
    },
    getSimpleScreen(handlerInput, content) {
        // Add APL Directive to response
        if (util.supportsAPL(handlerInput)) {
            handlerInput.responseBuilder.addDirective({
                type: 'Alexa.Presentation.APL.RenderDocument',
                version: '1.0',
                document: constants.APLDocs.simpleScreen,
                datasources: {
                    bodyTemplate1Data: {
                        type: "object",
                        objectId: "bt1Sample",
                        backgroundImage: {
                            contentDescription: null,
                            smallSourceUrl: null,
                            largeSourceUrl: null,
                            sources: [
                                {
                                    //url: handlerInput.t('LAYOUT_IMAGES_MSG'),
                                    url: handlerInput.t('LAUNCH_IMAGES_MSG'),
                                    size: "small",
                                    widthPixels: 0,
                                    heightPixels: 0
                                },
                                {
                                    //url: handlerInput.t('LAYOUT_IMAGES_MSG'),
                                    url: handlerInput.t('LAUNCH_IMAGES_MSG'),
                                    size: "large",
                                    widthPixels: 0,
                                    heightPixels: 0
                                }
                            ]
                        },
                        logoUrl: handlerInput.t('LOGO_MSG'),
                        title: handlerInput.t('LAUNCH_HEADER_MSG'),
                        textContent: {
                            primaryText: {
                                type: "PlainText",
                                text: (function () { constants.REPLACE.forEach(rep => { content = content.replace(new RegExp(rep, 'g'), ''); }); return content; })()
                            }
                        }
                    }
                }
            });
        }
    },
    getCardDetailScreen(handlerInput, data) {
        // Add APL Directive to response
        if (util.supportsAPL(handlerInput)) {
            handlerInput.responseBuilder.addDirective({
                type: 'Alexa.Presentation.APL.RenderDocument',
                version: '1.0',
                document: constants.APLDocs.cardSingleInfoScreen,
                datasources: {
                    bodyTemplate3Data: {
                        type: "object",
                        objectId: "bt3Sample",
                        backgroundImage: {
                            contentDescription: null,
                            smallSourceUrl: null,
                            largeSourceUrl: null,
                            sources: [
                                {
                                    url: handlerInput.t('LAYOUT_IMAGES_MSG'),
                                    size: "small",
                                    widthPixels: 0,
                                    heightPixels: 0
                                },
                                {
                                    url: handlerInput.t('LAYOUT_IMAGES_MSG'),
                                    size: "large",
                                    widthPixels: 0,
                                    heightPixels: 0
                                }
                            ]
                        },
                        logoUrl: handlerInput.t('LOGO_MSG'),
                        title: handlerInput.t('DETAILS_MSG'),
                        textContent: {
                            title: {
                                type: "PlainText",
                                text: (function () { constants.REPLACE.forEach(rep => { data.title = data.title.replace(new RegExp(rep, 'g'), ''); }); return data.title; })()
                            },
                            primaryText: {
                                type: "PlainText",
                                text: (function () { constants.REPLACE.forEach(rep => { data.content = data.content.replace(new RegExp(rep, 'g'), ''); }); return data.content; })()
                            }
                        }
                    }
                }
            });
        }
    },
    getVerticalGridScreen(handlerInput, dataItems) {
        // Add APL Directive to response
        if (util.supportsAPL(handlerInput)) {
            handlerInput.responseBuilder.addDirective({
                type: 'Alexa.Presentation.APL.RenderDocument',
                version: '1.0',
                document: constants.APLDocs.verticalGridScreen,
                datasources: {
                    listTemplate1Metadata: {
                        backgroundImage: {
                            contentDescription: null,
                            smallSourceUrl: null,
                            largeSourceUrl: null,
                            sources: [
                                {
                                    url: handlerInput.t('LAYOUT_IMAGES_MSG'),
                                    size: "small",
                                    widthPixels: 0,
                                    heightPixels: 0
                                },
                                {
                                    url: handlerInput.t('LAYOUT_IMAGES_MSG'),
                                    size: "large",
                                    widthPixels: 0,
                                    heightPixels: 0
                                }
                            ]
                        },
                        logoUrl: handlerInput.t('LOGO_MSG'),
                        title: handlerInput.t('LAUNCH_HEADER_MSG')
                    },
                    listTemplate1ListData: {
                        listPage: {
                            listItems: dataItems
                        }
                    }
                }
            });
        }
    },
    getMultiVerticalGridScreen(handlerInput, dataItems) {
        // Add APL Directive to response
        if (util.supportsAPL(handlerInput)) {
            handlerInput.responseBuilder.addDirective({
                type: 'Alexa.Presentation.APL.RenderDocument',
                version: '1.0',
                document: constants.APLDocs.multiGridScreen,
                datasources: {
                    listTemplate2Metadata: {
                        type: "object",
                        objectId: "lt1Metadata",
                        backgroundImage: {
                            contentDescription: null,
                            smallSourceUrl: null,
                            largeSourceUrl: null,
                            sources: [
                                {
                                    url: handlerInput.t('LAYOUT_IMAGES_MSG'),
                                    size: "small",
                                    widthPixels: 0,
                                    heightPixels: 0
                                },
                                {
                                    url: handlerInput.t('LAYOUT_IMAGES_MSG'),
                                    size: "large",
                                    widthPixels: 0,
                                    heightPixels: 0
                                }
                            ]
                        },
                        title: handlerInput.t('LAUNCH_HEADER_MSG'),
                        logoUrl: handlerInput.t('LOGO_MSG')
                    },
                    listTemplate2ListData: {
                        type: "list",
                        listId: "lt2Sample",
                        totalNumberOfItems: 10,
                        hintText: handlerInput.t('LAUNCH_HINT_MSG'),
                        listPage: {
                            listItems: dataItems
                        }
                    }
                }
            });
        }
    },
    getMultiVerticalGridDetailsScreen(handlerInput, dataItems) {
        // Add APL Directive to response
        if (util.supportsAPL(handlerInput)) {
            handlerInput.responseBuilder.addDirective({
                type: 'Alexa.Presentation.APL.RenderDocument',
                version: '1.0',
                document: constants.APLDocs.multiGridDetailsScreen,
                datasources: {
                    listTemplate2Metadata: {
                        type: "object",
                        objectId: "lt1Metadata",
                        backgroundImage: {
                            contentDescription: null,
                            smallSourceUrl: null,
                            largeSourceUrl: null,
                            sources: [
                                {
                                    url: handlerInput.t('LAYOUT_IMAGES_MSG'),
                                    size: "small",
                                    widthPixels: 0,
                                    heightPixels: 0
                                },
                                {
                                    url: handlerInput.t('LAYOUT_IMAGES_MSG'),
                                    size: "large",
                                    widthPixels: 0,
                                    heightPixels: 0
                                }
                            ]
                        },
                        title: handlerInput.t('LAUNCH_HEADER_MSG'),
                        logoUrl: handlerInput.t('LOGO_MSG')
                    },
                    listTemplate2ListData: {
                        type: "list",
                        listId: "lt2Sample",
                        totalNumberOfItems: 10,
                        hintText: handlerInput.t('LAUNCH_HINT_MSG'),
                        listPage: {
                            listItems: dataItems
                        }
                    }
                }
            });
        }
    }
}
